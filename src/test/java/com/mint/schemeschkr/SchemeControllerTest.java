package com.mint.schemeschkr;

import com.mint.schemeschkr.controller.SchemeController;
import com.mint.schemeschkr.domain.EType;
import com.mint.schemeschkr.domain.Payload;
import com.mint.schemeschkr.domain.Scheme;
import com.mint.schemeschkr.domain.Stat;
import com.mint.schemeschkr.service.ProducerService;
import com.mint.schemeschkr.service.SchemeService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SchemeController.class)
class SchemeControllerTest extends MockMvcBase {

    @MockBean
    private SchemeService schemeService;

    @MockBean
    private RestTemplateBuilder restTemplateBuilder;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private ProducerService producerService;

    @Test
    public void getStatsTest() throws Exception {

        Map<String, Long> payload = new HashMap<>();
        payload.put("545423", 5L);
        payload.put("679234", 4L);
        payload.put("329802", 1L);

        when(this.schemeService.count(any(Pageable.class))).thenReturn(new Stat(true, 1, 3, 133, payload));

        this.mockMvc.perform(MockMvcRequestBuilders
            .get("/card-scheme/stats")
            .param("start", "1")
            .param("limit", "3")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").isBoolean())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true))
            .andExpect(MockMvcResultMatchers.jsonPath("$.payload").exists());

    }

    @Test
    public void verfiyCardTest() throws Exception {

        when(this.schemeService.verify(anyInt())).thenReturn(new Scheme(true, new Payload("mastercard", EType.debit, "Zenith")));


        this.mockMvc.perform( MockMvcRequestBuilders
            .get("/card-scheme/verify/{no}", "234564")
            .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").isBoolean())
            .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true))
            .andExpect(MockMvcResultMatchers.jsonPath("$.payload").exists());

    }

}
