package com.mint.schemeschkr.controller;

import com.mint.schemeschkr.config.Generic;
import com.mint.schemeschkr.domain.Scheme;
import com.mint.schemeschkr.domain.Stat;
import com.mint.schemeschkr.service.ProducerService;
import com.mint.schemeschkr.service.SchemeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/card-scheme")
/**
 * REST controller for managing Schemes.
 */
public class SchemeController {

    private SchemeService schemeService;
    private ProducerService producerService;

    public SchemeController(SchemeService schemeService, ProducerService producerService) {
        this.schemeService = schemeService;
        this.producerService = producerService;
    }

    /**
     * Check schemes validity.
     *
     * @param no first 6-digits of your card
     * @return Scheme entity
     */
    @GetMapping("/verify/{no}")
    public Scheme verify(@PathVariable int no) {

        Scheme scheme = schemeService.verify(no);

        try {
            // Push payload to kafka
            producerService.send(Generic.KTOPIC, Generic.mapper().writeValueAsString(scheme.getPayload()));
        } catch (Exception e) {log.error("Something stinks ! \n{}", e.getMessage()); }

        return scheme;
    }

    /**
     * Get top hits card.
     *
     * @param start the start page
     * @param limit the payload limit
     * @return Stat entity
     */
    @GetMapping("/stats")
    public Stat count(@RequestParam(required = false, defaultValue = "1") int start, @RequestParam(required = false, defaultValue = "3") int limit) {

        return schemeService.count(PageRequest.of(start - 1, limit));
    }
}
