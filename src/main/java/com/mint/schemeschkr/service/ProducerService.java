package com.mint.schemeschkr.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProducerService {

    private KafkaTemplate<String, String> kafkaTemplate;

    public ProducerService(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     * Send request to Kafka Queue
     *
     * @param topic
     * @param data
     */
    public void send(String topic, String data) {
        log.info( "***sending data{} to {}", data, topic);
        kafkaTemplate.send(topic, data);
    }
}

