package com.mint.schemeschkr.service;

import com.mint.schemeschkr.domain.Scheme;
import com.mint.schemeschkr.domain.Stat;
import org.springframework.data.domain.Pageable;

public interface SchemeService {

    Scheme verify(int cardNo);

    Stat count(Pageable pageable);
}
