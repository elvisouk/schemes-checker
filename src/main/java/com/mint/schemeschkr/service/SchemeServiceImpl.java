package com.mint.schemeschkr.service;

import com.mint.schemeschkr.domain.*;
import com.mint.schemeschkr.repository.HitRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SchemeServiceImpl implements SchemeService {

    private SchemeAdapter schemeAdapter;
    private HitRepository hitRepository;

    public SchemeServiceImpl(SchemeAdapter schemeAdapter, HitRepository hitRepository) {
        this.schemeAdapter = schemeAdapter;
        this.hitRepository = hitRepository;
    }

    @Override
    @Cacheable(value="scheme", key="#cardNo")
    public Scheme verify(int cardNo) {

        log.info("Request to get card detail :: {}", cardNo);

        return schemeAdapter.transform(BinlistIntegrator.fetch(cardNo));
    }

    @Override
    public Stat count(Pageable pageable) {

        log.info("Request to get card Hits");

        return schemeAdapter.transform(hitRepository.findByCardnoGrouped(pageable));
    }
}

@Component
class SchemeAdapter {

    public Scheme transform(Optional<BinlistRecord> record) {
        if(record.isPresent()) {
            BinlistRecord bRecord = record.get();

            String scheme = bRecord.getScheme();
            EType type = bRecord.getType() == null ? EType.na : bRecord.getType();
            String bank = bRecord.getBank() == null ? "n/a" : bRecord.getBank().getName();

            return new Scheme(true, new Payload(scheme, type, bank));

        } else {
            return new Scheme(false, new Payload("n/a", EType.na, "n/a"));
        }
    }

    public Stat transform(Page<StatRecord> record) {
        Map<String, Long> payload = new LinkedHashMap<>();

        if(record.getContent().isEmpty()) {
            return new Stat(false, record.getNumber()+1, record.getSize(), record.getTotalElements(), payload);
        } else {
            payload = record.getContent()
                    .stream()
                    .collect(Collectors.toMap(StatRecord::getNo, StatRecord::getCounter, (o1,o2) -> o1, LinkedHashMap::new));

            return new Stat(true, record.getNumber()+1, record.getSize(), record.getTotalElements(), payload);
        }

    }

}
