package com.mint.schemeschkr.service;

import com.mint.schemeschkr.domain.BinlistRecord;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class BinlistIntegrator {

    private final static String URL = "https://lookup.binlist.net/";
    private static RestTemplate restTemplate;

    public BinlistIntegrator(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public static Optional<BinlistRecord> fetch(int cardNo) {

        return Optional.ofNullable(restTemplate.getForObject( URL + cardNo, BinlistRecord.class));
    }
}
