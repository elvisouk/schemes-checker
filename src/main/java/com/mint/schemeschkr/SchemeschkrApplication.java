package com.mint.schemeschkr;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class SchemeschkrApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SchemeschkrApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("........... Documentation :: http://localhost:8100/ ............ ");
    }
}
