package com.mint.schemeschkr.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mint.schemeschkr.util.RequestInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.jms.Queue;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@EnableCaching
@Configuration
public class Generic implements WebMvcConfigurer {

    public static final String QTOPIC = "hit.queue";
    public static final String KTOPIC = "com.ng.vela.even.card_verified";
    private static ObjectMapper mapper = null;

    @Autowired
    private JmsMessagingTemplate jms;

    @Value("${spring.kafka.bootstrap-servers}")
    private String kafkaBrokers;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        return builder.setConnectTimeout(Duration.ofMillis(300000))
                .setReadTimeout(Duration.ofMillis(300000)).build();
    }

    @Bean
    public Queue queue() {
        return new ActiveMQQueue(Generic.QTOPIC);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestInterceptor(jms, queue())).addPathPatterns("/card-scheme/verify/*");
    }

    @Bean
    public KafkaAdmin admin() {
        Map<String, Object> configs = new HashMap<>();
        log.info("*** Setting Up Kafka Broker {}", System.getenv("SPRING_KAFKA_BOOTSTRAP-SERVERS"));
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic createTopic() {
        log.info("*** Creating Topic {}", Generic.KTOPIC);
        return new NewTopic(Generic.KTOPIC, 3, (short) 1);
    }

    public static ObjectMapper mapper() {
        if(mapper == null) {
            mapper = new ObjectMapper();
        }

        return mapper;
    }

}
