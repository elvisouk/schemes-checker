package com.mint.schemeschkr.util;

import com.mint.schemeschkr.config.Generic;
import com.mint.schemeschkr.domain.Hit;
import com.mint.schemeschkr.repository.HitRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.jms.Queue;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class RequestInterceptor implements HandlerInterceptor {

    private JmsMessagingTemplate jms;
    private Queue queue;

    public RequestInterceptor(JmsMessagingTemplate jms, Queue queue) {
        this.jms = jms;
        this.queue = queue;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // async log
        this.jms.convertAndSend(this.queue, request.getRequestURI().split("/")[3]);
        return true;
    }
}

@Component
class MessageReceiver {

    @Autowired
    private HitRepository repository;

    @JmsListener(destination = Generic.QTOPIC)
    public void receiveQueue(String cardNo) {

        repository.save(new Hit(cardNo));

    }

}
