package com.mint.schemeschkr.repository;

import com.mint.schemeschkr.domain.Hit;
import com.mint.schemeschkr.domain.StatRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface HitRepository extends PagingAndSortingRepository<Hit, String> {

    @Query("select new com.mint.schemeschkr.domain.StatRecord(h.cardno as no, "
            + "count(h.id) as counter) "
            + "from Hit h "
            + "group by h.cardno order by counter desc")
    Page<StatRecord> findByCardnoGrouped(Pageable pageable);

}