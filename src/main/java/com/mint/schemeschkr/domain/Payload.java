package com.mint.schemeschkr.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Payload {

    /**
     * Card schemes
     */
    private String scheme;
    /**
     * Schemes type
     */
    private EType type;
    /**
     * Issuer bank
     */
    private String bank;
}