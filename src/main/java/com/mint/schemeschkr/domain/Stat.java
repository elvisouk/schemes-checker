package com.mint.schemeschkr.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Setter
@Getter
@AllArgsConstructor
public class Stat {

    /**
     * Scheme validity status
     */
    private boolean success;
    /**
     * Page start
     */
    private int start;
    /**
     * Payload item limit
     */
    private int limit;
    /**
     * Registered card size
     */
    private long size;
    /**
     * CardNo:Count map git spayload
     */
    private Map<String, Long> payload;

}
