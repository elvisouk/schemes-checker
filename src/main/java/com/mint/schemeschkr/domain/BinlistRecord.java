package com.mint.schemeschkr.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BinlistRecord {
    private String scheme;
    private EType type;
    private Bank bank;

    @Getter
    @Setter
    public static class Bank {
        private String name;
    }
}
