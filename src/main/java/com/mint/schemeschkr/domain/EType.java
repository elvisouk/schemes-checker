package com.mint.schemeschkr.domain;

public enum EType {
    credit,
    debit,
    na
}
