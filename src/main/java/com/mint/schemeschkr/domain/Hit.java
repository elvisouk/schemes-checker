package com.mint.schemeschkr.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "hits")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Hit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String cardno;

    private LocalDateTime logtime;

    public Hit(String cardno) {
        this.cardno = cardno;
    }

    @PrePersist
    public void logtime() {
        this.logtime = LocalDateTime.now();
    }
}
