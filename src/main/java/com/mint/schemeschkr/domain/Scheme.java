package com.mint.schemeschkr.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Scheme {

    /**
     * Scheme validity status
     */
    private boolean success;

    /**
     * Scheme payload
     */
    private Payload payload;
}
